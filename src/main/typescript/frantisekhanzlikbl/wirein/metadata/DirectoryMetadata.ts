
export default interface DirectoryMetadata {
	includes: string[]
	excludes: string[]
}
