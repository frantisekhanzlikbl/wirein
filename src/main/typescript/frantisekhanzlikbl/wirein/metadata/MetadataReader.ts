
import FileMetadata from "./FileMetadata";
import DirectoryMetadata from "./DirectoryMetadata";

export default interface MetadataReader {
	isFileMetadataFile(filePath: string): boolean
	isDirectoryMetadataFile(filePath: string): boolean
	getMetadataForFileMetadataFile(filePath: string): Promise<FileMetadata>
	getMetadataForDirectoryMetadataFile(filePath: string): Promise<DirectoryMetadata>
}
