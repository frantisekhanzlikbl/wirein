
export default interface FileMetadata {
	destination: string
	target: string
}
